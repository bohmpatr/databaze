#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_buttonBox_accepted()
{
 accept();
}


void Dialog::on_buttonBox_rejected()
{
    reject();
}

QString Dialog::nazev() const
{
    return ui->nazev->text();
}

QString Dialog::zanr() const
{
    return ui->zanr->text();
}

int Dialog::hodnoceni() const
{
    return ui->hodnoceni->value();

}
