#include "widget.h"
#include "ui_widget.h"
#include<dialog.h>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{

    ui->setupUi(this);
    QStringList nazvy;
    setWindowTitle("Databáze");
    // nastavim si tabulku
    ui->tableWidget->setColumnCount(3);

    nazvy<<"Název"<<"Žánr"<<"Hodnocení";

    // nastavim jmena do tabulky
    ui->tableWidget->setHorizontalHeaderLabels(nazvy);

}





Widget::~Widget()
{
    delete ui;
}


void Widget::on_tlacitko_clicked()
{   // vysledek, ktery se nam bude vracet
    int result, hodnoceni;
    QString nazev, zanr;


    Dialog pd(this);
    pd.setWindowTitle("Widget");
    result = pd.exec();
    if (result == QDialog::Rejected)
        return;
    // zazadam si o hodnoty
    nazev =  pd.nazev();
    zanr = pd.zanr();
    hodnoceni = pd.hodnoceni();
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, Nazev, new QTableWidgetItem(nazev));
    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, Zanr, new QTableWidgetItem(zanr));
    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, Hodnoceni, new QTableWidgetItem(QString::number(hodnoceni)));

}

